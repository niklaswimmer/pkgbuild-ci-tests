Testing how automatically building a Linux Kernel with Gitlab Pipelines would work and how a Gitlab Package repository could be used to distribute it.

Vagrant can be used to automatically create a Gitlab CI Runner in a local virtual machine. The configuration file located at `./gitlab-runner/config.toml`
needs to be adjusted first, especially the token value needs to be filled in. Then `vagrant up` can be run to launch the runner.
